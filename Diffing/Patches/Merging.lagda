\begin{code}
open import Prelude
open import Diffing.Universe.Syntax
open import Diffing.Universe.Equality
open import Diffing.Universe.MuUtils
open import Diffing.Patches.Diff
open import Diffing.Patches.Diff.Functor
open import Diffing.Patches.Overlap
open import Diffing.Patches.Residual
open import Diffing.Patches.Conflicts
open import Diffing.Patches.Merging.Grow

module Diffing.Patches.Merging where
\end{code}

